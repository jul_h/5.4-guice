package operations;

import interfaces.Command;

public class Subtraction implements Command {
    @Override
    public char getOperator() {
        return '-';
    }

    @Override
    public double execute(double a, double b) {
        return a - b;
    }
}
