package main;

import calculator.Calculator;
import com.google.inject.Guice;
import com.google.inject.Injector;
import modules.CalculatorModule;

public class Main {
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new CalculatorModule());
        Calculator calculator = injector.getInstance(Calculator.class);
        calculator.calculate(10, 20, '+');
        calculator.calculate(100, 20, '/');
        calculator.calculate(10, 3, '*');
        calculator.calculate(10, 3, '-');
        calculator.calculate(10.5, 3, '%');
    }
}
