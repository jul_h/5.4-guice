package modules;

import calculator.Calculator;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import interfaces.Command;
import operations.Addition;
import operations.Division;
import operations.Multiplication;
import operations.Subtraction;

import java.util.Arrays;
import java.util.List;

public class CalculatorModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Command.class).to(Addition.class);
        bind(Command.class).to(Division.class);
        bind(Command.class).to(Multiplication.class);
        bind(Command.class).to(Subtraction.class);
        bind(new TypeLiteral<List<Command>>(){})
                .toProvider(() -> Arrays.asList(
                        new Addition(),
                        new Subtraction(),
                        new Multiplication(),
                        new Division()));
        bind(Calculator.class);
    }
}
